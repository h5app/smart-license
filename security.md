## 安全加固
虽然通过 smart-license 生成的 License 具备防篡改的能力，
但对于具备一定技术能力的用户而言，依旧可以采用某些手段获得 License 的源数据，
再通过 smart-license 伪造一份"合法"的 License 文件。

为此需要提供一种安全加固策略，防止某些不怀好意的人过于轻松的突破 License 的安全防线。

加固的原理如下图所示，生成License阶段采用非对称加密方式对源数据进行预处理，
而在程序运行时从License中提取到的是密文形式的源数据，需要通过公钥解密还原真实内容。
![](images/security.png)

软件提供商大可将公钥硬编码至程序中，即便用户获得公钥，也无法以此伪造 License。
再则软件提供商在程序中引入代码混淆机制，增加反编译的破解难度，可以强化软件的安全程度。

### 加固步骤
1. 运行`${LICENSE_HOME}/bin/keypair.sh`生成秘钥对。

    ![](images/keypair.gif)
2. 执行成功后会在命令行打印生成的公钥 publicKey 和私钥 privateKey。
为了防止秘钥对遗失，会将其存储可以当前目录的 keystore 文件中。

    ![](images/keystore.png)
3. 使用`${LICENSE_HOME}/bin/license.sh`生成License的时候，传入私钥参数进行文件加密。

    ![](images/encrypt_data.png)
4. 加载解析License时传入公钥进行解密，强烈建议将公钥硬编码至项目代码中。

    ![](images/dencrypt_data.png)
